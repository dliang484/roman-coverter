package roman;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RomanConverterTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test(expected = IllegalArgumentException.class )
	public void lessThanZeroTest() {
		RomanConverter c = new RomanConverter();
		c.toRoman(-1);
		
	}

	@Test(expected = IllegalArgumentException.class )
	public void moreThan4000Test() {
		RomanConverter c = new RomanConverter();
		c.toRoman(4200);
		
	}
	
	@Test
	public void normalTest() {
		RomanConverter c = new RomanConverter();
		assertEquals("X",c.toRoman(10));

		
	}
	
	@Test(expected = IllegalArgumentException.class )
	public void invalidEntryTest() {
		RomanConverter c = new RomanConverter();
		c.fromRoman("U");
		
	}
	
	@Test
	public void validEntryTest() {
		RomanConverter c = new RomanConverter();
		assertEquals(10,c.fromRoman("X"));

		
	}
	
}
